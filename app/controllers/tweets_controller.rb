class TweetsController < ApplicationController
  def index
    byebug
    tweets = Tweet.by_infinite_scroll(params[:identifier])
    render json: tweets.all
  end

  private

  def search_params
    params.permit(:username)
  end
end
