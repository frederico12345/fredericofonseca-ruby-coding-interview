class Tweet < ApplicationRecord
  belongs_to :user

  scope :by_id_lt, ->(identifier) { where('id < ?', identifier) if identifier.present? }
  scope :by_infinite_scroll, proc { |identifier| by_id_lt(identifier).order(id: :desc).limit(5) }
end
