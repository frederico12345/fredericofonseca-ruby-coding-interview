require 'rails_helper'

RSpec.describe "Tweets", type: :request do
  describe "#index" do
    let!(:user_1) { create(:user) }
    let!(:user_2) { create(:user) }

    let!(:user_1_tweets) { create_list(:tweet, 10, user: user_1).reverse }

    let(:result) { JSON.parse(response.body) }

    context 'when fetching tweets' do
      it 'returns tweets using an infinity scroll sorted by the newest one first in groups of 5' do
        get tweets_path

        expect(result.size).to eq(5)
        expect(result.map { |t| t['id'] }).to eq(user_1_tweets[0..4].map(&:id))

        (0..user_1_tweets.size).each do |i|
          get tweets_path(identifier: user_1_tweets[i].id)

          expect(result.size).to eq(user_1_tweets[i..(i + 4)].size)
          expect(result.map { |t| t['id'] }).to eq(user_1_tweets[i..(i + 4)].map(&:id))
        end
      end
    end

    context 'when fetching tweets by username' do
      let!(:user_2_tweets) { create_list(:tweet, 10, user: user_2).reverse }

      it 'returns only the tweets for the specified user' do

      end
    end
  end
end
